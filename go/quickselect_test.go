package qsel

import (
	"testing"
)

func TestAsc(t *testing.T) {
	i := []int{1,2,3,4,5}
	k := QuickSelect(i, 2, Asc)
	if k != 2 {
		t.Fatal("bad quick select asc 2")
	}
	k = QuickSelect(i, 3, Asc)
	if k != 3 {
		t.Fatal("bad quick select asc 3")
	}
	t.Log(k)
}

func TestDesc(t *testing.T) {
	i := []int{1,2,3,4,5}
	k := QuickSelect(i, 2, Desc)
	if k != 4 {
		t.Fatal("bad quick select desc 4")
	}
	k = QuickSelect(i, 3, Asc)
	if k != 3 {
		t.Fatal("bad quick select desc 3")
	}
	t.Log(k)
}

func TestSliceList(t *testing.T) {
	i := []int{1,2,3,4,5}
	l := SliceListMaxN(i, 2)
	ll := []int{5, 4}
	for i:=0;i<2;i++ {
		if l[i] != ll[i] {
			t.Fatal("bad slice list max")
		}
	}
	l = SliceListMinN(i, 2)
	ll = []int{1, 2}
	for i:=0;i<2;i++ {
		if l[i] != ll[i] {
			t.Fatal("bad slice list min")
		}
	}
}

func TestSliceMap(t *testing.T) {
	i := map[string]int {
		"test1": 1,
		"test2": 2,
		"test3": 3,
		"test4": 4,
		"test5": 5,
	}
	ii := SliceMapMaxN(i, 2)
	mm := map[string]int {
		"test4": 4,
		"test5": 5,
	}
	for key, value := range(mm) {
		if ii[key] != value {
			t.Fatal("bad slice map max")
		}
	}
	ii = SliceMapMinN(i, 2)
	mm2 := map[string]int {
		"test1": 1,
		"test2": 2,
	}
	for key, value := range(mm2) {
		if ii[key] != value {
			t.Fatal("bad slice map min")
		}
	}
}
