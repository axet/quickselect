package qsel

type Less func(int, int) bool

func Desc(k int, i int) bool {
	return k > i
}

func Asc(k int, i int) bool {
	return k < i
}

func QuickSelect(list []int, k int, less Less) int {
	k = k - 1

	for {
		// partition
		px := len(list) / 2
		pv := list[px]
		last := len(list) - 1
		list[px], list[last] = list[last], list[px]
		i := 0
		for j := 0; j < last; j++ {
			if less(list[j], pv) {
				list[i], list[j] = list[j], list[i]
				i++
			}
		}
		// select
		if i == k {
			return pv
		}
		if k < i {
			list = list[:i]
		} else {
			list[i], list[last] = list[last], list[i]
			list = list[i+1:]
			k -= i + 1
		}
	}
}

func QuickSelectAsc(list []int, k int) int {
	return QuickSelect(list, k, Asc)
}

func QuickSelectDesc(list []int, k int) int {
	return QuickSelect(list, k, Desc)
}

func SliceListN(list []int, N int, less Less) []int {
	var TopN []int
	var TopNE []int

	if len(list) <= N {
		for key, _ := range list {
			TopN = append(TopN, key)
		}
	} else {
		V := QuickSelect(list, N, less)

		// we may have 1,1,1,1,2 counts, first take all above "1" then add ones
		for _, value := range list {
			if less(value, V) {
				TopN = append(TopN, value)
			}
			if len(TopN) >= N {
				return TopN
			}
			if value == V && len(TopNE) < N {
				TopNE = append(TopNE, value)
			}
		}
		// add ones
		for _, value := range TopNE {
			TopN = append(TopN, value)

			if len(TopN) >= N {
				return TopN
			}
		}
	}

	return TopN
}

func SliceMapN(mm map[string]int, N int, less Less) map[string]int {
	TopN := make(map[string]int)
	TopNE := make(map[string]int)

	if len(mm) <= N {
		for key, value := range mm {
			TopN[key] = value
		}
	} else {
		list := make([]int, len(mm))
		i := 0
		for _, value := range(mm) {
			list[i] = value
			i++
		}
		V := QuickSelect(list, N, less)

		// we may have 1,1,1,1,2 counts, first take all above "1" then add ones
		for key, value := range mm {
			if less(value, V) {
				TopN[key] = value
			}
			if len(TopN) >= N {
				return TopN
			}
			if value == V && len(TopNE) < N {
				TopNE[key] = value
			}
		}
		// add ones
		for key, value := range TopNE {
			TopN[key] = value

			if len(TopN) >= N {
				return TopN
			}
		}
	}

	return TopN
}

func SliceListMaxN(list []int, N int) []int {
	return SliceListN(list, N, Desc)
}

func SliceListMinN(list []int, N int) []int {
	return SliceListN(list, N, Asc)
}

func SliceMapMaxN(mm map[string]int, N int) map[string]int {
	return SliceMapN(mm, N, Desc)
}

func SliceMapMinN(mm map[string]int, N int) map[string]int {
	return SliceMapN(mm, N, Asc)
}
