# quickselect

Go library implements quickselect algorithm. Sort top n elements in a int array or map string->int

* func SliceListMaxN(list []int, N int) []int
* func SliceListMinN(list []int, N int) []int
* func SliceMapMaxN(mm map[string]int, N int) map[string]int
* func SliceMapMinN(mm map[string]int, N int) map[string]int
* func QuickSelectDesc(list []int, k int) int
* func QuickSelectAsc(list []int, k int) int


# Example

```go
package main

import "github.com/axet/quickselect/go"
import "fmt"

func main() {
  i := []int{1,2,3,4,5}
  m := qsel.SliceListMaxN(i, 3)
  fmt.Println(m)
}
```
